module gitlab.com/efronlicht/perfsample

go 1.23

require (
	github.com/spf13/afero v1.11.0
	gitlab.com/efronlicht/enve v1.1.0
)

require golang.org/x/text v0.20.0 // indirect
