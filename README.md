# perfsample
Scheduled performance sampling for go programs.
Easy to get started but highly customizable.

## docs
see https://godocs.io/gitlab.com/efronlicht/perfsample
## quick start / examples
See [examples_test.go](./examples_test.go)


## storage backends
`perfsample` uses the `afero.Fs` interface to store data.
I know of the following backends, but there may be more and you can implement your own.

| name | kind | repo | tested | 
| --- | ---| --- | --- |
| memory mapped | volatile in-memory | https://github.com/spf13/afero | ✅ | 
| OsFS | persistent (local disk) | https://github.com/spf13/afero | ✅ |
| google drive | persistent (cloud storage) | https://github.com/fclairamb/afero-gdrive |
| dropbox | persistent (cloud storage) | https://github.com/fclairamb/afero-dropbox | ☐ |
| sftp | persistent (remote disk) | https://github.com/spf13/afero |  ☐ |
| amazon s3 | persistent (cloud storage) | https://github.com/fclairamb/afero-s3 | ☐ |

Anything supporting the [afero.Fs](https://github.com/spf13/afero) 
