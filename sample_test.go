package perfsample

import (
	"context"
	"errors"
	"io"
	"log"
	"os"
	"path"
	"testing"
	"time"

	"github.com/spf13/afero"
	"gitlab.com/efronlicht/perfsample/sampling"

	"gitlab.com/efronlicht/perfsample/schedule"
)

func strPtr(s string) *string { return &s }
func TestCPUConfig_Validated(t *testing.T) {
	for _, tt := range []struct {
		name      string
		wantErrIs error
		cfg       CPUConfig
	}{
		{name: "need positive HowLong", wantErrIs: errNegativeCPUSamplingDuration, cfg: CPUConfig{HowLong: -1}},
		{name: "need scheduler", wantErrIs: errNilScheduler, cfg: CPUConfig{HowLong: time.Second}},
		{name: "bad dir", wantErrIs: errInvalidPath, cfg: CPUConfig{
			HowLong:   time.Second,
			Scheduler: schedule.After(2 * time.Second),
			Dir:       strPtr(`\\//asd./asl.dcasd\\ //`),
		}},
		{name: "just fine", wantErrIs: nil, cfg: CPUConfig{
			Scheduler: schedule.After(time.Minute),
		}},
	} {
		t.Run(tt.name, func(t *testing.T) {
			tt.cfg.Logger = log.New(io.Discard, "", 0)

			if _, err := tt.cfg.ValidatedWithDefaults(); !errors.Is(err, tt.wantErrIs) {
				t.Fatalf("expected error %v, but got %v", tt.wantErrIs, err)
			}
		})
	}
}

func TestConfig_Validated(t *testing.T) {
	s := schedule.Ticker(2 * time.Second)
	p := ProfileHeap
	for _, tt := range []struct {
		name      string
		wantErrIs error
		cfg       Config
	}{
		{name: "need profiler", wantErrIs: errBadProfile, cfg: Config{Scheduler: s}},
		{name: "need scheduler", wantErrIs: errNilScheduler, cfg: Config{Profiler: p}},
		{name: "bad dir", wantErrIs: errInvalidPath, cfg: Config{
			Profiler:  ProfileHeap,
			Scheduler: schedule.After(2 * time.Second),
			Dir:       strPtr("\\//asd./asl.dcasd\\ //"),
		}},
	} {
		t.Run(tt.name, func(t *testing.T) {
			tt.cfg.Logger = log.New(io.Discard, "", 0)

			if _, err := tt.cfg.ValidatedWithDefaults(); err == nil || !errors.Is(err, tt.wantErrIs) {
				t.Fatalf("expected error %v, but got %v", tt.wantErrIs, err)
			}
		})
	}
}

func TestScheduledProfiler_Run(t *testing.T) {
	t.Parallel()
	filesystem := afero.NewMemMapFs()
	for _, prof := range DefaultProfiles() {
		prof := prof

		ctx, cancel := context.WithCancel(context.Background())
		t.Run(string(prof), func(t *testing.T) {
			t.Parallel()
			ch := make(schedule.Manual, 1)
			go func() {
				defer cancel()
				start := time.Date(2021, time.September, 14, 0, 0, 0, 0, time.UTC)
				for i := time.Duration(0); i < 10; i++ {
					ch <- start.Add(time.Hour * i)
				}
			}()
			dir := path.Join("prof", string(prof))
			sampler, err := New(Config{
				Profiler:  prof,
				Scheduler: ch,
				FS:        filesystem,
				Strategy:  sampling.MostRecentLimitCount(5),
				Dir:       &dir,
				Locale:    time.UTC,
				Logger:    NoOpLogger,
			})
			if err != nil {
				t.Fatal(err)
			}
			kept, removed, err := sampler.Run(ctx)
			if err != nil {
				t.Fatal(err)
			}
			for _, k := range kept {
				if _, err := filesystem.Stat(path.Join(dir, k.Name())); err != nil {
					t.Errorf("error looking up %s: %v", k.Name(), err)
				}
				for _, r := range removed {
					if k.ModTime().Before(r.ModTime()) {
						t.Errorf("most recent strategy broken: %s kept before %s", k.ModTime(), r.ModTime())
					}
				}
			}
			for _, r := range removed {
				if _, err := filesystem.Stat(r.Name()); !errors.Is(err, os.ErrNotExist) {
					t.Errorf("expected err %v, but got %v", os.ErrNotExist, err)
				}
			}
		})
	}
}

func TestScheduledCPUProfiler_Run(t *testing.T) {
	if testing.Short() {
		t.Skip("SKIP: takes 10 cpu profiles for 100ms each")
	}
	t.Parallel()
	filesystem := afero.NewMemMapFs()
	ctx, cancel := context.WithCancel(context.Background())

	ch := make(schedule.Manual, 5)
	go func() {
		defer cancel()
		start := time.Date(2021, time.September, 14, 0, 0, 0, 0, time.UTC)
		for i := time.Duration(0); i < 10; i++ {
			ch <- start.Add(time.Hour * i)
		}
	}()
	dir := path.Join("prof", "cpu")
	sampler, err := NewCPU(CPUConfig{
		HowLong:   100 * time.Millisecond,
		Scheduler: ch,
		FS:        filesystem,
		Strategy:  sampling.MostRecentLimitCount(5),
		Dir:       strPtr(dir),
		Locale:    time.UTC,
		Logger:    NoOpLogger,
	})
	if err != nil {
		t.Fatal(err)
	}
	kept, removed, err := sampler.Run(ctx)
	if err != nil {
		t.Fatal(err)
	}
	for _, k := range kept {
		if _, err := filesystem.Stat(path.Join(dir, k.Name())); err != nil {
			t.Errorf("error looking up %s: %v", k.Name(), err)
		}
		for _, r := range removed {
			if k.ModTime().Before(r.ModTime()) {
				t.Errorf("most recent strategy broken: %s kept before %s", k.ModTime(), r.ModTime())
			}
		}
	}
	for _, r := range removed {
		if _, err := filesystem.Stat(r.Name()); !errors.Is(err, os.ErrNotExist) {
			t.Errorf("expected err %v, but got %v", os.ErrNotExist, err)
		}
	}
}
