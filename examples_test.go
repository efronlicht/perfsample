package perfsample_test

import (
	"context"
	"path/filepath"
	"runtime/debug"
	"time"

	"gitlab.com/efronlicht/enve"
	"gitlab.com/efronlicht/perfsample"
	"gitlab.com/efronlicht/perfsample/sampling"
	"gitlab.com/efronlicht/perfsample/schedule"
)

// Profile the heap exactly every five minutes until tomorrow.
// Error handling is omitted for brevity.
func ExampleScheduledProfiler_Run() {
	// Profile the heap every five minutes until tomorrow.
	// Error handling is omitted for brevity.
	go perfsample.MustNew(perfsample.Config{
		Profiler:  perfsample.ProfileHeap,
		Scheduler: schedule.Ticker(5 * time.Minute),
	}).Run(context.TODO())
}

// Profile the CPU for a minute about every hour, forever, keeping the last 1000 samples.
// Error handling is omitted for brevity.
func ExampleScheduledCPUProfiler_Run() {
	// Profile the CPU for a minute about every hour, forever, keeping the last 1000 samples.
	// Error handling is omitted for brevity.
	const mean, stdDev = time.Hour, 20 * time.Minute
	go perfsample.MustNewCPU(perfsample.CPUConfig{
		HowLong:   time.Minute,
		Strategy:  sampling.MostRecentLimitCount(1000),
		Scheduler: schedule.NewNormalDistribution(mean, stdDev),
	}).Run(context.TODO())
}

// Profile the CPU for a minute about every hour, using Reservoir sampling to keep approximately 20GiB of randomly distributed samples.
// Save the files using local time for the timestamps.
func ExampleScheduledCPUProfiler_Run_longTermSampling() {
	const mean, stdDev = time.Hour, 20 * time.Minute
	const GiB = 1024 * 1024 * 1024

	var dir string
	buildinfo, ok := debug.ReadBuildInfo()
	if ok {
		dir = filepath.Join("prof", enve.StringOr("APP_NAME", filepath.Base(buildinfo.Main.Path)), buildinfo.Main.Sum)
	} else {
		dir = filepath.Join("prof", "my_app", time.Now().Format(time.RFC3339))
	}
	go perfsample.MustNewCPU(perfsample.CPUConfig{
		HowLong:   time.Minute,
		Strategy:  sampling.ReservoirSampleBytes(20 * GiB),
		Scheduler: schedule.NewNormalDistribution(mean, stdDev),
		Dir:       &dir,
		Locale:    time.Local,
	}).Run(context.TODO())
}
