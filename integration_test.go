package perfsample

import (
	"testing"
)

func Test_Integration_Amazon_S3(t *testing.T) {
	skipNotImplemented(t)
}

func Test_Integration_Google_Drive(t *testing.T) {
	skipNotImplemented(t)
}

func Test_Integration_SFTP(t *testing.T) {
	skipNotImplemented(t)
}

func skipNotImplemented(t *testing.T) {
	t.Skipf("%s: SKIP: TODO", t.Name())
}
