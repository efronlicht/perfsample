package sampling_test

import (
	"fmt"
	"io/fs"
	"os"
	"sort"
	"testing"
	"time"

	"gitlab.com/efronlicht/perfsample/internal/fileinfo"
	"gitlab.com/efronlicht/perfsample/internal/testutil"

	"gitlab.com/efronlicht/perfsample/sampling"
)

var start = time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC)

func Test_Reservoir_LimitCount(t *testing.T) {
	testutil.SetupNonDeterministic(t)
	const n = 1_000_000
	const k = 100_000
	var buckets [100]int
	strategy := sampling.ReservoirSamplerLimitCount(k)
	stats := make([]fs.FileInfo, 0, k)
	for i := 0; i < n; i++ {
		t := start.Add(time.Duration(i) * time.Hour)
		fi := FakeFileInfo{name: t.Format(time.RFC3339), modTime: t}
		if _, err := time.Parse(time.RFC3339, fi.Name()); err != nil {
			panic(err)
		}
		stats, _ = strategy(stats, fi)
	}
	if len(stats) != k {
		t.Fatalf("wanted len(kept)==%d, len(omitted)=%d, but got %d", k, n-k, len(stats))
	}
	for _, fi := range stats {
		t, err := time.Parse(time.RFC3339, fi.Name())
		if err != nil {
			panic(err)
		}
		i := t.Sub(start) / time.Hour
		buckets[int(float64(i)/float64(n)*100)]++
	}

	const expected = k / len(buckets)
	const tol = expected / 10
	const min = expected - tol
	const max = expected + tol
	for i, count := range buckets {
		if count > max || count < min {
			t.Errorf("expected bucket %d to contain %d<count<%d, items but had %d", i, min, max, count)
		}
	}
}

// this test is not deterministic and could fail given
// a very strange seed to the rng.
// that said, I've tested it with `go test --count 2000`, so I feel pretty confident.
func Test_Reservoir_LimitBytes(t *testing.T) {
	testutil.SetupNonDeterministic(t)
	const (
		GiB           = 1024 * 1024 * 1024
		targetSize    = 2 * GiB
		maxSize       = 4 * GiB
		smallFileSize = targetSize / 8
		largeFileSize = targetSize / 2
	)
	start := time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC)
	strategy := sampling.ReservoirSampleBytes(targetSize)
	var all, kept []fs.FileInfo
	// fill it up with "small" samples
	for i := 0; i < 8; i++ {
		fi := FakeFileInfo{
			name:    fmt.Sprintf("heap_%06d.prof", i),
			modTime: start.Add(time.Duration(i) * 24 * time.Hour),
			size:    smallFileSize,
		}
		all = append(all, fi)
		kept, _ = strategy(kept, fi)
		if len(kept) != len(all) {
			t.Fatalf("strategy should not have discarded anything yet, since it should have just %d bytes", i*smallFileSize)
		}
	}
	// start pumping in larger samples.
	// the size should remain under maxSize.
	// in theory, we could keep failing to sample the new ones,
	// so we do a _ton_ to make  sure this test isn't flaky.
	oldLen := len(kept)
	for i := 8; i < 128; i++ {
		fi := FakeFileInfo{
			name:    fmt.Sprintf("heap_%06d.prof", i),
			modTime: start.Add(time.Duration(i) * 24 * time.Hour),
			size:    largeFileSize,
		}
		var removed []fs.FileInfo
		all = append(all, fi)
		kept, removed = strategy(kept, fi)
		if len(removed) == 0 {
			t.Fatalf("should have removed at least one element")
		}
		var currentSize int64
		for _, k := range kept {
			currentSize += k.Size()
		}
		if currentSize > maxSize {
			t.Fatalf("currentSize (%d) should never go above maxSize (%d)", currentSize, maxSize)
		}
	}
	if len(kept) >= oldLen {
		t.Fatalf("expected the resize to result in a smaller number of samples")
	}
}

func Test_KeepMostRecentLimitBytes(t *testing.T) {
	t.Parallel()
	const (
		GiB           = 1024 * 1024 * 1024
		targetSize    = 2 * GiB
		maxSize       = 4 * GiB
		smallFileSize = targetSize / 8
		largeFileSize = targetSize / 4
	)
	start := time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC)
	strategy := sampling.MostRecentLimitByteSize(targetSize)
	var all, kept []fs.FileInfo
	// fill it up with "small" samples
	for i := 0; i < 8; i++ {
		fi := FakeFileInfo{
			name:    fmt.Sprintf("heap_%06d.prof", i),
			modTime: start.Add(time.Duration(i) * 24 * time.Hour),
			size:    smallFileSize,
		}
		all = append(all, fi)
		kept, _ = strategy(kept, fi)
		if len(kept) != len(all) {
			t.Fatalf("strategy should not have discarded anything yet, since it should have just %d bytes", i*smallFileSize)
		}
	}
	// start pumping in larger samples.
	// the size should remain under maxSize.
	oldLen := len(kept)
	for i := 8; i < 16; i++ {
		fi := FakeFileInfo{
			name:    fmt.Sprintf("heap_%06d.prof", i),
			modTime: start.Add(time.Duration(i) * 24 * time.Hour),
			size:    largeFileSize,
		}
		var removed []fs.FileInfo
		all = append(all, fi)
		kept, removed = strategy(kept, fi)
		if len(removed) == 0 {
			t.Fatalf("should have removed at least one element")
		}
		var currentSize int64
		for _, k := range kept {
			currentSize += k.Size()
			for _, r := range removed {
				if r.ModTime().After(k.ModTime()) {
					t.Fatalf(
						"removed newer sample %s (modTime: %s) before older sample %s: (modTime: %s)",
						r.Name(),
						r.ModTime().Format(time.RFC3339),
						k.Name(),
						k.ModTime().Format(time.RFC3339),
					)
				}
			}
		}
		if currentSize > maxSize {
			t.Fatalf("currentSize (%d) should never go above maxSize (%d)", currentSize, maxSize)
		}
	}
	if len(kept) >= oldLen {
		t.Fatalf("expected the resize to result in a smaller number of samples")
	}
}

func Test_KeepMostRecent(t *testing.T) {
	t.Parallel()
	var all, stats []fs.FileInfo
	start := time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC)
	const k = 6
	strategy := sampling.MostRecentLimitCount(k)
	for i := 0; i < 12; i++ {
		fi := FakeFileInfo{name: fmt.Sprintf("heap_%06d.prof", i), modTime: start.Add(time.Duration(i) * 5 * time.Minute)}
		all = append(all, fi)
		stats, _ = strategy(stats, fi)
	}
	if len(stats) != k {
		t.Fatalf("expected %d, got %d", k, len(stats))
	}
	sort.Sort(fileinfo.ByModTime(stats))
	sort.Sort(fileinfo.ByModTime(all))

	for i := range stats {
		if stats[i].Name() != all[i+k].Name() {
			t.Errorf("expected %v, but got %v", all[i+k].Name(), stats[i].Name())
		}
	}
}

var _ fs.FileInfo = FakeFileInfo{}

type FakeFileInfo struct {
	size    int64
	name    string
	modTime time.Time
}

func (ffi FakeFileInfo) Name() string       { return ffi.name }
func (ffi FakeFileInfo) Size() int64        { return ffi.size }
func (ffi FakeFileInfo) ModTime() time.Time { return ffi.modTime }

// stub. gives zero value.
func (ffi FakeFileInfo) Mode() os.FileMode { return 0 }

// stub. gives zero value.
func (ffi FakeFileInfo) IsDir() bool { return false }

// stub. gives zero value.
func (ffi FakeFileInfo) Sys() interface{} { return nil }
