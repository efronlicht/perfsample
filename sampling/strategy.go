// Choose if and how to discard old samples using sampling.Strategy. Strategies include KeepAll (default), Resivoir Sampling, and Most Recent.
package sampling

import (
	"fmt"
	"io/fs"
	"math/rand/v2"
	"slices"
	"sort"

	"gitlab.com/efronlicht/perfsample/internal/fileinfo"
)

// Strategy chooses samples to keep or remove from a list.
// it is mandatory for len(keep) + len(remove) == len(samples) + 1.
// In general, these are impure functions (closures) meant for a single ScheduledProfiler or ScheduledCPUProfiler.
type Strategy func(samples []fs.FileInfo, toAdd fs.FileInfo) (keep, remove []fs.FileInfo)

// ReservoirSamplerLimitCount chooses a simple random sample without replacement of k items from an unknown number of items in a single pass.
// This uses math/rand/v2's RNG as the source of randomness.
// The resulting Strategy is not safe for re-use across multiple profilers: make a new one each time.
func ReservoirSamplerLimitCount(k int) Strategy {
	var initialized bool
	var count int
	return func(samples []fs.FileInfo, s fs.FileInfo) (keep, remove []fs.FileInfo) {
		defer func() { count++ }()
		if !initialized {
			count = len(samples)
			initialized = true
		}
		if len(samples) < k {
			return append(samples, s), nil
		}
		if rand.Float64() < (float64(k) / float64(count)) {
			i := int(rand.Int64N(int64(k)))
			samples[i], s = s, samples[i]
		}
		return samples, []fs.FileInfo{s}
	}
}

// KeepAll is a Strategy that never removes anything.
// This is safe for re-use.
func KeepAll(samples []fs.FileInfo, toAdd fs.FileInfo) (keep, remove []fs.FileInfo) {
	return append(samples, toAdd), nil
}

// MostRecentLimitCount is a Strategy that keeps the k most recent samples.
// The resulting Strategy is not safe for re-use across multiple profilers: make a new one each time.
func MostRecentLimitCount(k int) Strategy {
	if k <= 0 {
		panic(fmt.Sprintf("expected k > 0, but got k = %d", k))
	}
	alreadySorted := false
	queueBack := 0
	return func(samples []fs.FileInfo, s fs.FileInfo) (keep, remove []fs.FileInfo) {
		if len(samples) < k {
			return append(samples, s), nil
		}
		// no more room. we need to start treating this as a queue.
		if !alreadySorted {
			sort.Sort(fileinfo.ByModTime(samples))
			alreadySorted = true
		}

		samples[queueBack], s = s, samples[queueBack]
		// increment, wrapping if necessary
		queueBack = (queueBack + 1) % k
		return samples, []fs.FileInfo{s}
	}
}

// MostRecentLimitByteSize is a Strategy that keeps the most recent samples, trying to
// stay around targetBytes. For implementation reasons, this may use up to 2*targetBytes,
// but probably won't.
// The resulting Strategy is not safe for re-use across multiple profilers: make a new one each time.
func MostRecentLimitByteSize(targetBytes int64) Strategy {
	if targetBytes <= 0 {
		panic(fmt.Sprintf("expected targetBytes>0, but got targetBytes = %d", targetBytes))
	}
	var currentBytes int64
	initialized := false
	queueBack := 0
	return func(samples []fs.FileInfo, s fs.FileInfo) (keep, remove []fs.FileInfo) {
		if !initialized {
			initialized = true
			sort.Sort(fileinfo.ByModTime(samples))
			for _, s := range samples {
				currentBytes += s.Size()
			}
		}
		// if we haven't hit our target yet, just add it.
		if b := s.Size(); b+currentBytes <= targetBytes {
			currentBytes += b
			return append(samples, s), nil
		}

		// if we have, replace the oldest sample with the new one.
		queueBack %= len(samples)
		currentBytes = currentBytes + s.Size() - samples[queueBack].Size()
		samples[queueBack], s = s, samples[queueBack]
		queueBack++

		// we're in the acceptable range.
		if currentBytes < 2*targetBytes {
			return samples, []fs.FileInfo{s}
		}

		// we're way beyond our target.
		samples = append(samples, s)

		slices.SortFunc(samples, func(a, b fs.FileInfo) int { return -a.ModTime().Compare(b.ModTime()) }) // newest first.
		// put the newest entries FIRST so that we can return a contiguous slice.
		sort.Sort(sort.Reverse(fileinfo.ByModTime(samples)))
		var i int
		// swap-remove the oldest samples until we are under targetBytes.
		for i = len(samples) - 1; currentBytes > targetBytes; i-- {
			currentBytes -= samples[i].Size()
			if i < 0 {
				panic("positive currentBytes despite no samples left. this should be impossible.")
			}
		}

		keep, remove = samples[:i], samples[i:]
		// restore the queue.
		queueBack = 0

		slices.Reverse(keep)
		return keep, remove
	}
}

// ReservoirSampleBytes chooses a simple random sample without replacement
// from an unknown distribution with total filesize of approximately targetBytes.
// For implementation reasons, this may use up to 2*targetBytes.
// The resulting Strategy is not safe for re-use across multiple profilers: make a new one each time.
func ReservoirSampleBytes(targetBytes int64) Strategy {
	if targetBytes <= 0 {
		panic(fmt.Sprintf("expected targetBytes>0, but got targetBytes = %d", targetBytes))
	}
	var initialized bool
	var count int
	var currentBytes int64
	return func(samples []fs.FileInfo, s fs.FileInfo) (keep, remove []fs.FileInfo) {
		defer func() { count++ }()

		// this is the first time, so we haven't accounted for
		// out what the sizes of pre-existing samples are yet.
		if !initialized {
			initialized = true
			for _, smp := range samples {
				currentBytes += smp.Size()
			}
		}

		// if we haven't hit our target yet, just add it.
		if b := s.Size(); b+currentBytes <= targetBytes {
			currentBytes += b
			return append(samples, s), nil
		}
		// if we have hit our target, use Reservoir sampling to decide how to collect new ones.
		if rand.Float64() < (float64(len(samples)) / float64(count)) {
			i := int(rand.Int64N(int64(len(samples))))
			currentBytes = currentBytes + s.Size() - samples[i].Size()
			samples[i], s = s, samples[i]
		}
		// we're still in the acceptable range.
		if currentBytes < 2*targetBytes {
			return samples, []fs.FileInfo{s}
		}
		// we're way too big. start throwing things away until we are under targetSize.
		// shuffle so that our choice of which samples to remove is unbiased.
		rand.Shuffle(len(samples), func(i, j int) { samples[i], samples[j] = samples[j], samples[i] })
		for i := len(samples) - 1; i >= 0; i-- {
			currentBytes -= samples[i].Size()
			if currentBytes <= targetBytes {
				return samples[:i], samples[i:]
			}
		}
		panic("positive currentBytes despite no samples left. this should be impossible.")
	}
}
