package perfsample

// internal implementation details share between ScheduledCPUProfiler and ScheduledProfiler.
import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path"
	"sync/atomic"
	"time"

	"github.com/spf13/afero"
	"gitlab.com/efronlicht/perfsample/internal"
	"gitlab.com/efronlicht/perfsample/internal/fileinfo"
	"gitlab.com/efronlicht/perfsample/sampling"
	"gitlab.com/efronlicht/perfsample/schedule"
)

// DefaultFileMode is 0o666: all users can Read and Write, but not Execute.
const DefaultFileMode = 0o666

var (
	_ Logger = (*log.Logger)(nil)
	// NoOpLogger does nothing.
	NoOpLogger Logger = noOpLogger{}

	errNegativeCPUSamplingDuration = errors.New("negative CPU sampling duration")
	errNilScheduler                = errors.New("nil scheduler")
	errInvalidPath                 = errors.New("invalid path")
	errBadProfile                  = errors.New("invalid or empty profiler")
)

// _scheduledProfiler contains the shared underlying structure of ScheduledCPUProfiler and ScheduledProfiler
type (
	_scheduledProfiler struct {
		// it is undefined behavior for any of these fields to be the zero value except
		// internal.NoCopy (zero-sized type), dstDir and _started.

		internal.NoCopy
		_started int32 // 0 or 1 only. used for atomic.CompareAndSwap.

		scheduler schedule.Scheduler // Config.Scheduler or CPUConfig.Scheduler
		strategy  sampling.Strategy  // Config.Strategy or CPUConfig.Strategy
		dstDir    string             // Config.Dir or CPUConfig.Dir
		fileMode  fs.FileMode        // Config.FileMode or CPUConfig.FileMode
		fs        afero.Fs           // Config.FS or CPUConfig.FS
		locale    *time.Location     // Config.Locale or CPUConfig.Locale
		logger    Logger             // Config.Logger CPUConfig.Logger
	}
	// Logger logs. Satisfied by NoOpLogger and *log.Logger, among others.
	Logger interface {
		Printf(format string, args ...interface{})
	}
	// noOpLogger is a Logger whose Printf does nothing.
	noOpLogger struct{}
)

func (p *_scheduledProfiler) setupDirAndLoadOldSamples(profileName string) (starting []fs.FileInfo, err error) {
	if !atomic.CompareAndSwapInt32(&p._started, 0, 1) {
		panic("Run() called more than once!")
	}
	if err := p.fs.MkdirAll(p.dstDir, 0o666); err != nil && !errors.Is(err, os.ErrExist) {
		return nil, fmt.Errorf("could not create directory '%s': %w", p.dstDir, err)
	}
	stat, err := p.fs.Stat(p.dstDir)
	if err != nil {
		return nil, fmt.Errorf("could not open path '%s': %w", p.dstDir, err)
	}
	if !stat.IsDir() {
		return nil, fmt.Errorf("error: expected path '%s' to be a dir: %w", p.dstDir, err)
	}
	inDir, err := afero.ReadDir(p.fs, p.dstDir)
	if err != nil {
		return nil, fmt.Errorf("getting list of files in directory '%s': %w", p.dstDir, err)
	}
	// find existing profiles in this namespace and load them: they still count,
	// even if they're from a previous run.
	wantExt := "." + profileName + "_prof"
	for _, f := range inDir {
		name := f.Name()
		// files from a previous run of this profiler should contain an RFC3339 timestamp and have an extension matching Profiler.Ext().
		if base := path.Base(name); fileinfo.ContainsRFC3339.MatchString(base) && path.Ext(name) == wantExt {
			starting = append(starting, f)
		}
	}
	return starting, nil
}

// DefaultLogger inherits the standard logger from the log package, but adds a prefix "<PROFILER>_prof " (note whitespace).
// eg, "heap_prof ", "cpu_prof "
func DefaultLogger(profileName string) *log.Logger {
	logger := log.Default()
	prefix := profileName + "_prof "
	if formerPrefix := logger.Prefix(); formerPrefix != "" {
		prefix = formerPrefix + " " + prefix
	}
	logger.SetPrefix(prefix)
	return logger
}

// Printf does nothing.
func (noOpLogger) Printf(string, ...interface{}) {}

// check the bounds of the scheduled profiler, and that the context is non-nil.
// with the exception of nil context and filemode errors, none of these errors should be possible during runtime.
func (p *_scheduledProfiler) checkBounds(ctx context.Context) error {
	switch {
	// these errors are possible due to user error
	case ctx == nil: // user provided nil context
		return errors.New("nil context")
	case p.fileMode == 0 || p.fileMode > 0o777: // user provided impossible fileMode
		return fmt.Errorf("invalid filemode: expected %O<filemode<=%O, but got %O", 0o000, 0o777, p.fileMode)
	case p == nil: // user accidentally called checkBounds() on a nil ScheduledProfiler or CPUProfiler
		return errors.New("nil _scheduledProfiler")
	// these ones are my fault.
	case p.scheduler == nil:
		return errors.New("nil scheduler" + messageOnImpossiblePanic)
	case p.strategy == nil:
		return errors.New("nil strategy" + messageOnImpossiblePanic)
	case p.fs == nil:
		return errors.New("nil fs" + messageOnImpossiblePanic)
	case p.locale == nil:
		return errors.New("nil locale" + messageOnImpossiblePanic)
	case p.logger == nil:
		return errors.New("nil logger" + messageOnImpossiblePanic)
	default:
		return nil
	}
}
