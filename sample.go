// Package perfsample schedules performance profiling via the ScheduledProfiler and ScheduledCPUProfiler structs.
// see examples_test.go for examples.
package perfsample

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	_ "runtime"
	"runtime/pprof"
	"time"

	"github.com/spf13/afero"
	"gitlab.com/efronlicht/perfsample/sampling"
	"gitlab.com/efronlicht/perfsample/schedule"
)

type (
	// ScheduledProfiler can Run a Profiler at the times given by a schedule.Scheduler,
	// saving it under an Identifier to an afero.Fs, and using a sampling.Strategy to choose how to retain or discard samples.
	// Build it with New or MustNew.
	// A ScheduledProfiler should only be Run once and should never be copied.
	ScheduledProfiler struct {
		_scheduledProfiler
		profile *pprof.Profile // corresponds directly to Config.Profiler.
		debug   int            // provided to *pprof.Profile.WriteTo(w, debug)
	}
	// Profiler is the kind of profile to run. Each constant (ProfileHeap, ProfileBlock, etc) directly corresponds to one of the default
	// profilers in runtime/pprof, via pprof.Lookup().
	Profiler string

	// Config builds a ScheduledProfiler via New() or MustNew().
	Config struct {
		// Profiler to run. Directly corresponds to the profilers in runtime/pprof. Mandatory.
		// see https://pkg.go.dev/runtime/pprof#Profile
		Profiler Profiler
		// Scheduler chooses when to schedule the next profiling event. Mandatory.
		Scheduler schedule.Scheduler
		// Strategy chooses if and how to discard old samples. Optional [Defaults to sampling.KeepAll].
		Strategy sampling.Strategy
		// Dir to save profiles to on the FS. Optional [Defaults to "prof/<Profiler>". eg: "prof/heap"].
		// I strongly suggest at least putting the profiler kind, application name, and commit or release in the path
		// since it will help you get useful historical data and
		// will make sure the sampling.Strategy doesn't throw out unrelated profiles.
		// Starting with "prof" is just convention.
		// example: chess server
		//
		// 		// prof/$APP_NAME/$RELEASE
		//		prof/chess_server/v1.0.0
		//
		// example: local batch processing application
		//	 	// prof/$APP_NAME/$COMMIT
		//		prof/batch_processor/1e47f02010367d6acaa5c5ee2d27405a1e8339a9
		//
		// example: aws cloud deployment w/ many different kinds of instances
		//		// prof/$APP_NAME/$COMMIT/$ARCH/$OS/$GO_VERSION/$HOST_MACHINE/
		//		prof/incredibly_complicated_cloud_brain_thing/1e47f02010367d6acaa5c5ee2d27405a1e8339a9/arm64/linux/go1.17.1/aws_m6g_medium/
		Dir *string
		// FileMode is the mode to use while creating new files. Optional. [Defaults to 0o666].
		FileMode fs.FileMode
		// FS is the destination filesystem. Optional. [Defaults to afero.OsFs]
		FS afero.Fs
		// Logger to use. Optional. [Defaults to DefaultLogger(Profiler)]
		// Disable logging entirely with NoOpLogger.
		Logger Logger

		// Locale to use for timestamps in RFC3339.  Optional. [Defaults to time.UTC].
		// In general, I suggest you use UTC instead of local time.
		Locale *time.Location

		// Debug is the argument passed to runtime/pprof.Profile.WriteTo. Optional. [Defaults to 1 - human-readable legacy text format].
		// To quote the documentation at https://pkg.go.dev/runtime/pprof#Profile.WriteTo:
		// 	// The debug parameter enables additional output.
		//	// Passing debug=0 writes the gzip-compressed protocol buffer described in https://github.com/google/pprof/tree/master/proto#overview.
		//  // Passing debug=1 writes the legacy text format with comments translating addresses to function names and line numbers, so that a programmer can read the profile without tools.
		// 	// The predefined profiles may assign meaning to other debug values; for example, when printing the "goroutine" profile,
		//  // debug=2 means to print the goroutine stacks in the same form that a Go program uses when dying due to an unrecovered panic.
		Debug *int
	}
)

const (
	// ProfileGoRoutine is the heap profiler, corresponding to pprof.Lookup("goroutine")
	ProfileGoRoutine Profiler = "goroutine"
	// ProfileHeap is the heap profiler, corresponding to pprof.Lookup("heap")
	ProfileHeap Profiler = "heap"
	// ProfileAllocs is the allocation profiler, corresponding to pprof.Lookup("allocs")
	ProfileAllocs Profiler = "allocs"
	// ProfileThreadCreate is the thread creation profiler, corresponding to pprof.Lookup("threadcreate")
	ProfileThreadCreate Profiler = "threadcreate"
	// ProfileBlock is the blocking profiler, corresponding to pprof.Lookup("block").
	// This doesn't work unless the rate passed to runtime.SetBlockProfileRate() is nonzero.
	ProfileBlock Profiler = "block"
	// ProfileMutex is the mutex profiler, corresponding to pprof.Lookup("mutex").
	// This doesn't work unless the rate passed to runtime.SetMutexProfileFraction(rate) is nonzero.
	ProfileMutex Profiler = "mutex"
)

// DefaultProfiles returns an array with all the default profiles. Corresponds directly to pprof.Profiles().
func DefaultProfiles() [6]Profiler {
	return [...]Profiler{
		ProfileGoRoutine,
		ProfileHeap,
		ProfileAllocs,
		ProfileThreadCreate,
		ProfileBlock,
		ProfileMutex,
	}
}

// MustNew is as New, but panics on error.
func MustNew(cfg Config) *ScheduledProfiler {
	sp, err := New(cfg)
	if err != nil {
		panic(err)
	}
	return sp
}

// New builds a ScheduledProfiler from Config.
// A non-nil error means a valid sampler.
// The Config.Profiler, Config.ID, and Config.Scheduler fields are mandatory.
func New(cfg Config) (*ScheduledProfiler, error) {
	cfg, err := cfg.ValidatedWithDefaults()
	if err != nil {
		return nil, fmt.Errorf("invalid configuration: %w", err)
	}
	var sp ScheduledProfiler
	sp.profile = pprof.Lookup(string(cfg.Profiler))
	sp.logger = cfg.Logger
	sp.dstDir = *cfg.Dir // guaranteed non-nil by ValidatedWithDefaults().
	sp.fs = cfg.FS
	sp.locale = cfg.Locale
	sp.strategy = cfg.Strategy
	sp.scheduler = cfg.Scheduler
	sp.fileMode = cfg.FileMode
	sp.debug = *cfg.Debug // guaranteed non-nil by ValidatedWithDefaults().
	return &sp, nil
}

// Run the scheduled profiler until the context is cancelled.
// Since this function blocks indefinitely, you probably want to run it in a goroutine.
// Run will panic if called more than once per instance or if the context is nil.
func (sp *ScheduledProfiler) Run(ctx context.Context) (kept, removed []fs.FileInfo, err error) {
	if err := sp.checkBounds(ctx); err != nil {
		panic(fmt.Errorf("%T.Run(): checking bounds: %w", sp, err))
	}

	starting, err := sp.setupDirAndLoadOldSamples(sp.profile.Name())
	if err != nil {
		return nil, nil, fmt.Errorf("setting up %s profile: %w", sp.profile.Name(), err)
	}
	sp.logger.Printf("starting %s profile", sp.profile.Name())
	// this channel should be closed when the context is done.
	i := 0
	kept = starting
	for t := range sp.scheduler.Schedule(ctx) {
		time.Sleep(time.Until(t))
		i++
		sample, err := sp.writeSample(t, sp.debug)
		if err != nil {
			return kept, removed, fmt.Errorf("writing sample #%04d: %w", i, err)
		}
		var toRemove []os.FileInfo
		kept, toRemove = sp.strategy(kept, sample)
		for _, f := range toRemove {
			err := sp.fs.Remove(f.Name())
			if err != nil && !errors.Is(err, os.ErrNotExist) {
				sp.logger.Printf("error: failed to remove profile at path '%s': %v", f.Name(), err)
			}
		}
		removed = append(removed, toRemove...)
	}
	return kept, removed, nil
}

func (sp *ScheduledProfiler) writeSample(t time.Time, debug int) (stat fs.FileInfo, err error) {
	timestamp := t.In(sp.locale).Format(time.RFC3339)
	name := filepath.Join(sp.dstDir, fmt.Sprintf("%s.%s_prof", timestamp, sp.profile.Name()))
	sp.logger.Printf("writing sample %s", name)

	// Open in write-only mode. If it doesn't exist, create it first.
	// If it does, truncate the existing file before writing.
	const flag = os.O_CREATE | os.O_WRONLY | os.O_TRUNC
	f, err := sp.fs.OpenFile(name, flag, sp.fileMode)
	if err != nil {
		return nil, fmt.Errorf("writing sample : creating or truncating profile file at path '%s': %w", name, err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			sp.logger.Printf("writeSample error: failed to close file at path '%s': %v", f.Name(), err)
		}
	}()
	if err := sp.profile.WriteTo(f, debug); err != nil {
		return stat, fmt.Errorf("writing to profile at path '%s': %w", name, err)
	}
	return f.Stat()
}

// ValidatedWithDefaults returns a copy of Config with the defaults set.
// err != nil means one or more mandatory fields were not set
// or a field was invalid.
func (cfg Config) ValidatedWithDefaults() (Config, error) {
	profile := pprof.Lookup(string(cfg.Profiler))
	if cfg.Profiler == "" || profile == nil {
		return cfg, fmt.Errorf("%w %q", errBadProfile, cfg.Profiler)
	}
	var err error
	defaultDir := fmt.Sprintf("prof/%s", cfg.Profiler)
	if cfg.Dir, err = validateDir(cfg.Dir, defaultDir); err != nil {
		return cfg, fmt.Errorf("validating directory: %w", err)
	}
	if cfg.Logger == nil {
		cfg.Logger = DefaultLogger(string(cfg.Profiler))
		cfg.Logger.Printf("no logger provided: using default logger")
	}
	if cfg.Scheduler == nil {
		return cfg, errNilScheduler
	}
	if cfg.FileMode == 0 {
		cfg.FileMode = DefaultFileMode
		cfg.Logger.Printf("no filemode provided: using default %O", DefaultFileMode)
	}
	if cfg.Debug == nil {
		debug := 1
		cfg.Debug = &debug
		cfg.Logger.Printf("no debug toggle provided: using default (1 - human readable legacy text format)")
	}
	if cfg.Locale == nil {
		cfg.Locale = time.UTC
		cfg.Logger.Printf("no locale provided: using default locale (UTC)")
	}
	if cfg.Strategy == nil {
		cfg.Strategy = sampling.KeepAll
		cfg.Logger.Printf("no sampling strategy provided: defaulting to KeepAll")
	}

	if cfg.FS == nil {
		cfg.FS = afero.NewOsFs()
		cfg.Logger.Printf("no filesystem provided: defaulting to OsFs")
	}
	cfg.Logger.Printf("configuration is valid")
	return cfg, nil
}
