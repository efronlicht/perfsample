package testutil

import (
	"testing"

	"gitlab.com/efronlicht/enve"
)

func SetupNonDeterministic(t *testing.T) {
	t.Helper()
	if testing.Short() {
		t.Skipf("SKIP %s: takes a long time and non-deterministic", t.Name())
	}
	if !enve.BoolOr("PERFSAMPLE_TEST_NONDETERMINISTIC", false) {
		t.Skipf(`SKIP %s: PERFSAMPLE_TEST_NONDETERMINISTIC is missing or false
HELP: set PERFSAMPLE_TEST_NONDETERMINISTIC=true to run this test`, t.Name())
	}
	t.Parallel()
}
