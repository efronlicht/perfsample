package internal

import (
	"sync"
)

var _ sync.Locker = NoCopy{}

// NoCopy indicates a struct should not be copied.
type NoCopy struct{}

func (NoCopy) Lock()   {}
func (NoCopy) Unlock() {}
