package fileinfo

import (
	"io/fs"
	"regexp"
)

// ContainsRFC3339 is a regex that matches what's probably a RFC3339 timestamp.
// `YYYY-MM-DDTHH:MM:SS(timezone)`
// We just use this to match, not to parse.
var ContainsRFC3339 = regexp.MustCompile(`(\d\d\d\d-\d\d-\d\d[T ]\d\d:\d\d:\d\d)(Z|[\+-]\d\d:\d\d)?`)

// ByModTime implements sort.Sort by chronologically sorting files by (fs.FileInfo).ModTime().
type ByModTime []fs.FileInfo

func (bmt ByModTime) Less(i, j int) bool { return bmt[i].ModTime().Before(bmt[j].ModTime()) }
func (bmt ByModTime) Swap(i, j int)      { bmt[i], bmt[j] = bmt[j], bmt[i] }
func (bmt ByModTime) Len() int           { return len(bmt) }
