package perfsample

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"runtime"
	"runtime/pprof"
	"strings"
	"time"

	"github.com/spf13/afero"
	"gitlab.com/efronlicht/perfsample/sampling"
	"gitlab.com/efronlicht/perfsample/schedule"
)

type (
	// ScheduledCPUProfiler will run the cpu profile (pprof.StartCPUProfile and pprof.StopCPUProfile) according to a schedule.Scheduler,
	// saving it under an Identifier to an afero.Fs, and using a sampling.Strategy to choose how to retain or discard samples.
	// Build it with NewCPU or MustNewCPU.
	ScheduledCPUProfiler struct {
		_scheduledProfiler
		howLong time.Duration // CPUConfig.HowLong
	}

	// CPUConfig builds a ScheduledCPUProfiler via NewCPU() or MustNewCPU().
	CPUConfig struct {
		// HowLong to profile the CPU during each sample. Optional. [Defaults to 30*time.Second].
		HowLong time.Duration
		// Scheduler chooses when to schedule the next profiling event. Mandatory.
		// It is undefined behavior for a new sampling event to arrive when the cpu profiler is already running,
		// so make sure HowLong is less than the minimum interval between schedules.
		// I will probably figure out a consistent behavior for this later.
		Scheduler schedule.Scheduler
		// Strategy chooses if and how to discard old samples. Optional. [Defaults to sampling.KeepAll].
		Strategy sampling.Strategy
		// Dir to save profiles to on the FS. Optional. [Defaults to "/prof/<Profiler>"].
		// I strongly suggest at least putting the application name & commit or release in the path.
		// Starting with "prof" is just convention.
		//
		// example: chess server
		//
		// 		// prof/$APP_NAME/$RELEASE
		//		"prof/chess_server/v1.0.0"
		//
		// example: local batch processing application
		//	 	// prof/$APP_NAME/$COMMIT
		//		"prof/batch_processor/1e47f02010367d6acaa5c5ee2d27405a1e8339a9"
		//
		// example: aws cloud deployment w/ many different kinds of instances
		//		// prof/$APP_NAME/$COMMIT/$ARCH/$OS/$GO_VERSION/$HOST_MACHINE/
		//		"prof/hellish_cloud_deploy/1e47f02010367d6acaa5c5ee2d27405a1e8339a9/arm64/linux/go1.17.1/aws_m6g_medium"
		Dir *string
		// FileMode is the mode to use while creating new files. Optional. [Defaults to DefaultFileMode: 0o666: everyone can READ and WRITE, but not execute]
		FileMode fs.FileMode
		// FS is the destination filesystem. Optional. [Defaults to afero.OsFs]
		// TODO: test integrations for AWS, GCP, Dropbox, Azure, SFTP.
		FS afero.Fs
		// Logger to use. Optional. [Defaults to DefaultLogger("cpu")]
		// Disable logging entirely with NoOpLogger.
		Logger Logger

		// Locale to use for timestamps in RFC3339.  Optional. [Defaults to time.UTC].
		// In general, I suggest you use UTC instead of local time.
		Locale *time.Location
	}
)

// NewCPU builds a ScheduledCPUProfiler from Config.
// A non-nil error means a valid sampler.
// The Config.Profiler, Config.ID, and Config.Scheduler fields are mandatory.
func NewCPU(cfg CPUConfig) (scp *ScheduledCPUProfiler, err error) {
	cfg, err = cfg.ValidatedWithDefaults()
	if err != nil {
		return nil, fmt.Errorf("invalid configuration: %w", err)
	}
	scp = new(ScheduledCPUProfiler)
	scp.howLong = cfg.HowLong
	scp.fileMode = cfg.FileMode
	scp.logger = cfg.Logger
	scp.dstDir = *cfg.Dir // guaranteed non-nil by cfg.ValidatedWithDefaults().
	scp.fs = cfg.FS
	scp.locale = cfg.Locale
	scp.strategy = cfg.Strategy
	scp.scheduler = cfg.Scheduler
	return scp, nil
}

func validateDir(dirp *string, fallback string) (*string, error) {
	if dirp == nil {
		return &fallback, nil
	}
	dir := *dirp
	dir = strings.TrimPrefix(dir, ".")
	dir = strings.TrimSuffix(dir, "/")
	if dir[0] == '/' {
		// fs.ValidPath doesn't allow rooted paths, but some filesystems may be OK with this,
		// so let's not reject it out of hand.
		if !fs.ValidPath(dir[1:]) {
			return nil, fmt.Errorf("%w: %s", errInvalidPath, *dirp)
		}
		return &dir, nil
	}
	if !fs.ValidPath(dir) {
		return nil, fmt.Errorf("%w: %s", errInvalidPath, *dirp)
	}
	return &dir, nil
}

// ValidatedWithDefaults returns a copy of CPUConfig with the defaults set.
// err != nil means one or more mandatory fields were not set
// or a field was invalid.
func (cfg CPUConfig) ValidatedWithDefaults() (CPUConfig, error) {
	const defaultDir = "prof/cpu"
	var err error
	if cfg.Dir, err = validateDir(cfg.Dir, defaultDir); err != nil {
		return cfg, fmt.Errorf("validating directory: %w", err)
	}
	if cfg.Logger == nil {
		cfg.Logger = DefaultLogger("cpu")
		cfg.Logger.Printf("no logger provided: using default logger")
	}
	if cfg.HowLong < 0 {
		return cfg, errNegativeCPUSamplingDuration
	}
	if cfg.HowLong == 0 {
		const defaultHowLong = 30 * time.Second
		cfg.Logger.Printf("no duration provided for HowLong: using default duration %s", defaultHowLong)
		cfg.HowLong = defaultHowLong
	}
	if cfg.FileMode == 0 {
		cfg.Logger.Printf("no filemode provided: using default filemode %O", DefaultFileMode)
		cfg.FileMode = DefaultFileMode
	}

	if cfg.Scheduler == nil {
		return cfg, errNilScheduler
	}

	if cfg.Locale == nil {
		cfg.Logger.Printf("no locale provided: using default locale (UTC)")
		cfg.Locale = time.UTC
	}
	if cfg.Strategy == nil {
		cfg.Logger.Printf("no sampling strategy provided: defaulting to sampling.KeepAll")
		cfg.Strategy = sampling.KeepAll
	}

	if cfg.FS == nil {
		cfg.FS = afero.NewOsFs()
		cfg.Logger.Printf("no filesystem provided: defaulting to afero.NewOsFs()")
	}
	cfg.Logger.Printf("configuration is valid")
	return cfg, nil
}

// MustNewCPU is as NewCPU, but panics on error.
func MustNewCPU(cfg CPUConfig) *ScheduledCPUProfiler {
	scp, err := NewCPU(cfg)
	if err != nil {
		panic(err)
	}
	return scp
}

func (scp ScheduledCPUProfiler) writeSample(t time.Time) (stat fs.FileInfo, err error) {
	timestamp := t.In(scp.locale).Format(time.RFC3339)
	// like "2006-01-02T15:04:05Z07:00_amd64_linux_go1.16.1.cpu_prof"
	name := filepath.Join(scp.dstDir, fmt.Sprintf("%s_%s_%s_%s.cpu_prof", timestamp, runtime.GOOS, runtime.GOARCH, runtime.Version()))
	scp.logger.Printf("writing sample %s", name)

	// open in write-only mode. if it doesn't exist, create it first.
	// if it does, truncate the existing file before writing.
	const flag = os.O_CREATE | os.O_WRONLY | os.O_TRUNC
	f, err := scp.fs.OpenFile(name, flag, scp.fileMode)
	if err != nil {
		return nil, fmt.Errorf("creating or truncatingprofile file at path '%s': %w", name, err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			scp.logger.Printf("error: failed to close file at path '%s': %v", f.Name(), err)
		}
	}()

	if err := pprof.StartCPUProfile(f); err != nil {
		return stat, fmt.Errorf("starting CPU profile at path '%s': %w", name, err)
	}
	time.Sleep(scp.howLong)
	pprof.StopCPUProfile()
	return f.Stat()
}

// Run the ScheduledCPUProfiler until the ctx is done.
// Since this function blocks indefinitely, you probably want to run it in a goroutine.
// Run will panic if called more than once per instance or if the context is nil.
func (scp *ScheduledCPUProfiler) Run(ctx context.Context) (kept, removed []fs.FileInfo, err error) {
	const profile = "cpu"
	if err := scp.checkBounds(ctx); err != nil {
		panic(fmt.Errorf("%T.Run(): checking bounds: %w", scp, err)) // this should be impossible!
	}
	starting, err := scp.setupDirAndLoadOldSamples(profile)
	if err != nil {
		return nil, nil, fmt.Errorf("setting up cpu profile: %w", err)
	}
	fmt.Println("foo")
	scp.logger.Printf("starting %s profile", profile)
	// this channel should be closed when the context is done.
	i := 0
	kept = starting
	for t := range scp.scheduler.Schedule(ctx) {
		time.Sleep(time.Until(t))
		i++
		sample, err := scp.writeSample(t)
		if err != nil {
			return kept, removed, fmt.Errorf("writing sample #%04d: %w", i, err)
		}
		var toRemove []os.FileInfo
		kept, toRemove = scp.strategy(kept, sample)
		for _, f := range toRemove {
			err := scp.fs.Remove(f.Name())
			if err != nil && !errors.Is(err, os.ErrNotExist) {
				scp.logger.Printf("error: failed to remove profile at path '%s': %v", f.Name(), err)
			}
		}
		removed = append(removed, toRemove...)
	}
	return kept, removed, nil
}
