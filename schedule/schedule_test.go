package schedule

import (
	"context"
	"testing"
	"time"

	"gitlab.com/efronlicht/perfsample/internal/testutil"
)

func TestNormalDistribution_Schedule(t *testing.T) {
	testutil.SetupNonDeterministic(t)
	const mean, stddev = 5 * time.Millisecond, time.Millisecond
	ch := NewNormalDistribution(mean, stddev).Schedule(context.Background())
	const samples = 10_000
	prev := <-ch

	var withinDeviations [4]int
	for i := 0; i < samples; i++ {
		if i%1000 == 0 && i != 0 {
			for j := range withinDeviations {
				t.Logf("%6d/%6d within %d std deviations, %.3f", withinDeviations[j], i, j+1, float64(withinDeviations[j])/float64(i))
			}
		}
		now := <-ch

		offset := now.Sub(prev) - mean - (mean / 10)
		prev = now
		if offset < 0 {
			offset = -offset
		}
		if offset < stddev {
			withinDeviations[0]++
		} else if offset < stddev*2 {
			withinDeviations[1]++
		} else if offset < stddev*3 {
			withinDeviations[2]++
		} else {
			withinDeviations[3]++
		}
	}
	if ratio := float64(withinDeviations[0]) / samples; ratio < .63 || ratio > .73 {
		t.Fatal("1 std dev", ratio)
	} else if ratio = ratio + (float64(withinDeviations[1]) / samples); ratio < .92 || ratio > .98 {
		t.Fatal("2 std devs")
	}
}

func Test_ExactFinite_Schedule(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		now := time.Now()
		var events [5]time.Time
		for i := range events {
			events[i] = now.Add(100 * time.Millisecond)
		}
		reversed := events
		// reverse the order of the events
		reversed[4], reversed[3], reversed[1], reversed[0] = events[0], events[1], events[3], events[4]
		// they should still show up in chronological order
		var i int
		for event := range Exact(reversed[:]).Schedule(context.Background()) {
			if !events[i].Equal(event) {
				t.Fatal("wrong order")
			}
			i++
		}
		if i != 5 {
			t.Fatal("wrong number of events")
		}
	})
	t.Run("cancelled early", func(t *testing.T) {
		now := time.Now()
		var events [5]time.Time
		for i := range events {
			events[i] = now.Add(100 * time.Millisecond)
		}
		ctx, cancel := context.WithCancel(context.Background())
		cancel()
		_, ok := <-Exact(events[:]).Schedule(ctx)
		if ok {
			t.Fatal("should have been closed before any events were sent")
		}
	})
}

func Test_LinearDistribution_Cancellation(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	cancel()
	const min, max = 5 * time.Millisecond, 10 * time.Millisecond
	_, ok := <-NewLinearDistribution(min, max).Schedule(ctx)
	if ok {
		t.Fatal("should have been closed before any events were sent")
	}
}

func Test_NormalDistribution_Cancellation(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	cancel()
	const mean, stddev = time.Second, 10 * time.Millisecond
	_, ok := <-NewNormalDistribution(mean, stddev).Schedule(ctx)
	if ok {
		t.Fatal("should have been closed before any events were sent")
	}
}

func Test_LinearDistribution_HappyPath(t *testing.T) {
	testutil.SetupNonDeterministic(t)
	const min, max = 5 * time.Millisecond, 10 * time.Millisecond
	ch := NewLinearDistribution(min, max).Schedule(context.Background())
	var quintiles [5]int
	prev := <-ch
	for i := 0; i < 10_000; i++ {
		if i%1000 == 0 {
			t.Log(quintiles)
		}
		now := <-ch
		// compensate for timing jitter:
		// it's always going to take slightly longer than we'd hoped
		// because of go's scheduler.
		if d := now.Sub(prev) - (min + (min / 20)); d < time.Millisecond {
			quintiles[0]++
		} else if d < 2*time.Millisecond {
			quintiles[1]++
		} else if d < 3*time.Millisecond {
			quintiles[2]++
		} else if d < 4*time.Millisecond {
			quintiles[3]++
		} else {
			quintiles[4]++
		}
		prev = now
	}
	const expected = 10_000 / 5
	const tol = expected / 5
	for i, q := range quintiles {
		if q < expected-tol || q > expected+tol {
			t.Errorf("expected quintile %d to contain %d +/- %d items, but had %d", i, expected, tol, q)
		}
	}
	t.Fail()
}

func TestTimer(t *testing.T) {
	t.Parallel()
	ctx, cancel := context.WithTimeout(context.Background(), 200*time.Millisecond)
	defer cancel()
	ch := After(time.Millisecond).Schedule(ctx)
	if _, ok := <-ch; !ok {
		t.Fatal("expected the channel not to be closed")
	}

	if _, ok := <-ch; ok {
		t.Fatal("expected the channel  to be closed")
	}
}

func TestTicker(t *testing.T) {
	testutil.SetupNonDeterministic(t)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	var count int
	ch := Ticker(time.Millisecond).Schedule(ctx)
	for range ch {
		count++
	}
	if count > 1200 || count < 800 {
		t.Fatal(count)
	}
}

func TestBoth_Schedule(t *testing.T) {
	testutil.SetupNonDeterministic(t)
	a := make(Manual, 2)
	b := make(Manual, 2)

	ctx, cancel := context.WithCancel(context.Background())
	out := Both{a, b}.Schedule(ctx)
	a <- time.Time{}
	a <- time.Time{}
	b <- time.Time{}
	for i := 0; i < 3; i++ {
		if _, ok := <-out; !ok {
			t.Fail()
		}
	}
	cancel()
	if _, ok := <-out; ok {
		t.Error("expected channel to be closed")
	}

	if _, ok := <-a; ok {
		t.Error("expected channel to be closed")
	}
	if _, ok := <-b; ok {
		t.Error("expected channel to be closed")
	}
}
