// Schedule when to take performance samples using a Scheduler.
// Some options include:
//
// - A deterministic, fixed number of samples via `Exact` or `After`
//
// - A regular schedule via `Ticker“
//
// - Semi-randomized distribution via `NormalDistribution' or `LinearDistribution`
package schedule

import (
	"context"
	"sort"
	"time"
)

var (
	_ Scheduler      = (*NormalDistribution)(nil)
	_ Scheduler      = (*LinearDistribution)(nil)
	_ Scheduler      = Ticker(0)
	_ Scheduler      = After(0)
	_ Scheduler      = Exact(nil)
	_ Scheduler      = Manual(nil)
	_ sort.Interface = Exact(nil)
	_ Scheduler      = Both{}
)

type (
	// Scheduler schedules events by sending times on a channel.
	Scheduler interface {
		// Schedule zero or more events.
		// It should respect context.Done() by immediately closing the channel it creates.
		// The channel should never be nil.
		// Schedule() should panic immediately on a nil context.
		Schedule(ctx context.Context) <-chan time.Time
	}
	// Manual is a Scheduler you can just shove events in.
	// Schedule() launches a goroutine that closes the channel when the context is done.
	// Don't close the underlying channel: that should be the context's job.
	// Not safe for re-use: call Schedule() only once.
	// This is mostly meant for tests.
	Manual chan time.Time

	// Exact is a Scheduler who sends its events in chronological order,
	// at the times listed. Events that are in the past when Schedule() is called will be sent immediately.
	// Each call to Schedule() will create an independent channel.
	// Cancelling the context will stop further events.
	// Exact fulfills sort.Interface by using (time.Time).Before().
	Exact []time.Time

	// Ticker creates a Scheduler that delivers “ticks” of a clock at intervals using time.Ticker.
	// A new time.Ticker is created for every call to Schedule().
	// A negative or zero value will panic on Schedule().
	Ticker time.Duration

	// After creates a Scheduler that waits for the duration to elapse,
	// and then sends the current time on the returned channel, using a time.Timer.
	// A new time.Timer is created for every call to Schedule().
	// A negative or zero value will panic on Schedule()
	After time.Duration

	// Both combines the output of any two other schedulers, sending out events as they come in.
	// No particular ordering is guaranteed.
	// Safe for re-use if and only if both L and R are safe for re-use.
	// L or R being nil will panic on Schedule().
	Both struct {
		L, R Scheduler
	}
)

// Schedule combines the output of (L).Schedule() and (R).Schedule() ending out events as they come in.
// No particular ordering is guaranteed.
// Safe for re-use if and only if both L and R are safe for re-use.
func (b Both) Schedule(ctx context.Context) <-chan time.Time {
	assertCtxNonNil(b, ctx)
	if b.L == nil {
		panic("L is nil")
	}
	if b.R == nil {
		panic("R is nil")
	}
	childCtx, cancelChild := context.WithCancel(context.Background())
	left, right := b.L.Schedule(childCtx), b.R.Schedule(childCtx)
	assertChannelNonNil(b.L, left)
	assertChannelNonNil(b.R, right)
	out := make(chan time.Time, 2)
	go func() { // merge the output of left and right and send them to out.
		defer func() { // cleanup.
			cancelChild()
			close(out)
			// we're all done. drain the channels.
			for range left {
				// deliberate no-op.
			}
			for range right {
				// deliberate no-op.
			}
		}()
		for {
			// if either channel closes, we no longer need to worry about the other one.
			select {
			case t, ok := <-left:
				if !ok {
					pipeChan(ctx, out, right)
					return
				}
				out <- t
			case t, ok := <-right:
				if !ok {
					pipeChan(ctx, out, left)
					return
				}
				out <- t
			case <-ctx.Done():
				return
			}
		}
	}()
	return out
}

// Schedule forwards the output of the channel, closing it when the context is done.
func (m Manual) Schedule(ctx context.Context) <-chan time.Time {
	assertCtxNonNil(m, ctx)
	if m == nil {
		panic("nil channel")
	}
	go func() {
		<-ctx.Done()
		close(m)
	}()
	return m
}

// Schedule events at the times listed, in chronological order, until out of events or the context is done.
// Events that are already expired when Schedule() is first called will be sent as soon as possible.
func (ex Exact) Schedule(ctx context.Context) <-chan time.Time {
	assertCtxNonNil(ex, ctx)
	sort.Sort(ex)
	out := make(chan time.Time, 1)
	go func() {
		defer close(out)
		for _, t := range ex {
			select {
			case <-ctx.Done():
				return
			case <-time.After(time.Until(t)):
				out <- t
			}
		}
	}()
	return out
}

// Schedule delivers 'ticks' of a clock at intervals via time.Ticker.
// (*time.Ticker).Stop will be called when the context is cancelled, and the returned channel will be closed.
func (t Ticker) Schedule(ctx context.Context) <-chan time.Time {
	assertCtxNonNil(t, ctx)
	assertPositiveDuration("Ticker", time.Duration(t))
	out := make(chan time.Time, 1)
	tick := time.NewTicker(time.Duration(t))
	go func() {
		for {
			select {
			case t := <-tick.C:
				out <- t
			case <-ctx.Done():
				tick.Stop()
				close(out)
				return
			}
		}
	}()
	return out
}

// Schedule forwards the results via time.After.
// Stop will be called when the context is cancelled, and the returned channel will be closed.
func (a After) Schedule(ctx context.Context) <-chan time.Time {
	assertCtxNonNil(a, ctx)
	assertPositiveDuration("After", time.Duration(a))
	out := make(chan time.Time, 1)
	go func() {
		defer close(out)
		select {
		case <-ctx.Done():
			return
		case t := <-time.After(time.Duration(a)):
			out <- t
			return
		}
	}()
	return out
}

// pipe src to dst until src or ctx is closed.
func pipeChan(ctx context.Context, dst chan<- time.Time, src <-chan time.Time) {
	for {
		select {
		case <-ctx.Done():
			return
		case t, ok := <-src:
			if !ok {
				return
			}
			dst <- t
		}
	}
}

// Len helps implement sort.Interface by returning the length.
func (ex Exact) Len() int { return len(ex) }

// Swap helps implement sort.Interface by swapping elements.
func (ex Exact) Swap(i, j int) { ex[i], ex[j] = ex[j], ex[i] }

// Less helps implement sort.Interface by calling (time.Time).Before()
func (ex Exact) Less(i, j int) bool { return ex[i].Before(ex[j]) }
