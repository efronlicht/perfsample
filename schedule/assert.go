package schedule

import (
	"context"
	"fmt"
	"time"
)

func assertCtxNonNil(s Scheduler, ctx context.Context) {
	if ctx == nil {
		panic(fmt.Sprintf("nil ctx passed to %T.Schedule()", s))
	}
}

func assertChannelNonNil(s Scheduler, ch <-chan time.Time) {
	if ch == nil {
		panic(fmt.Sprintf("%T.Schedule() has nil channel", s))
	}
}

func assertPositiveDuration(name string, t time.Duration) {
	if t <= 0 {
		panic(fmt.Sprintf("expected %s to be a positive duration, but got %s", name, t))
	}
}
