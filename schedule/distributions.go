package schedule

import (
	"context"
	"math/rand/v2"
	"time"
)

type (
	// LinearDistribution is a Scheduler that schedules events at an interval randomly chosen each time between min and max.
	// Events are scheduled indefinitely until the context is done.
	// Build with NewLinearDistribution or NewLinearDistributionFromSource.
	// It is OK to re-use the same LinearDistribution for multiple schedulers: they use thread-local RNGs.
	LinearDistribution struct {
		min, max time.Duration
	}
	// NormalDistribution is a Scheduler that schedules events normally distributed around mean and stdDev.
	// Events are scheduled indefinitely until the context is done.
	// Build with NewNormalDistribution or NewNormalDistributionFromSources.
	// It is OK to re-use the same NormalDistribution for multiple schedulers; they use thread-local RNGs.
	NormalDistribution struct {
		stddev, mean time.Duration
	}
)

var _, _ Scheduler = (*LinearDistribution)(nil), (*NormalDistribution)(nil)

// NewLinearDistribution creates a Scheduler that schedules events at an interval randomly chosen each time between min and max.
// Events are scheduled indefinitely until the context is done.
// Not safe to share between threads.
// Expects 0 <= min < max and panics otherwise.
func NewLinearDistribution(min, max time.Duration) *LinearDistribution {
	assertPositiveDuration("min", min)
	assertPositiveDuration("max", max)
	return &LinearDistribution{
		min: min,
		max: max,
	}
}

// NewNormalDistribution returns a Scheduler that schedules events normally (i.e, in a bell curve) around mean and stddev until the context is done.
// Expects 0 < mean && 0 < stdDev and panics otherwise.
// Not safe to share between threads: make a NewNormalDistribution
// for each scheduler.
func NewNormalDistribution(mean, stdDev time.Duration) Scheduler {
	assertPositiveDuration("mean", mean)
	assertPositiveDuration("stdDev", stdDev)
	return &NormalDistribution{
		mean:   mean,
		stddev: stdDev,
	}
}

// Schedule is the implementation of Scheduler.Schedule(). Events are linearly (i.e, evenly) distributed around the given mean and stddev.
func (ld *LinearDistribution) Schedule(ctx context.Context) <-chan time.Time {
	return scheduleDistribution(ctx, ld.sleepTime)
}

func (ld *LinearDistribution) sleepTime() time.Duration {
	return time.Duration(rand.N(ld.max-ld.min)) + ld.min
}

// Schedule is the implementation of Scheduler.Schedule(). Events are normally distributed around the given mean and stddev.
func (nd *NormalDistribution) Schedule(ctx context.Context) <-chan time.Time {
	return scheduleDistribution(ctx, nd.sleepTime)
}

func (nd *NormalDistribution) sleepTime() time.Duration {
	return max(time.Duration(rand.NormFloat64())*nd.stddev+nd.mean, 0)
}

func scheduleDistribution(ctx context.Context, sleepTime func() time.Duration) <-chan time.Time {
	out := make(chan time.Time, 1)
	go func() {
		timer := time.NewTimer(sleepTime())
		defer func() {
			if !timer.Stop() {
				<-timer.C
			}
			close(out)
		}()
		for {
			select {
			case <-ctx.Done():
				return
			case t := <-timer.C:
				out <- t
				timer.Reset(sleepTime())
			}
		}
	}()
	return out
}
