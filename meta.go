package perfsample

import "fmt"

const (
	BuildTime string = `2021-10-08 12:22:56-07:00`
	Version   string = "0.1.0"
)

var messageOnImpossiblePanic = fmt.Sprintf(
	`author note: this error should be impossible!
this is a bug in the program. if you see it, 
please contact me with details of how this occurred.
email me at efron.dev@gmail.com or file an issue at 
	https://gitlab.com/efronlicht/perfsample.
please include any details of how this occurred, 
including the build time and version of this release.
build time: %s
version: %s
`, BuildTime, Version)
